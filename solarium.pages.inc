<?php

/**
 * @file
 * Page callbacks that return data about the Solr server.
 */

use Solarium\Client;
use Solarium\QueryType\Luke\Query as LukeQuery;
use Solarium\QueryType\StatsJsp\Query as StatsJspQuery;
use Solarium\QueryType\System\Query as SystemQuery;

/**
 * Page callback; Delivers a JSON payload with data retrieved from Solr.
 *
 * @param string $function
 *   The function that returns the requested data. The return value must either
 *   be an array or an object that has a getData() method that returns an array.
 * @param string $command
 *   The command run by Drupal's AJAX system.
 */
function solarium_ajax_page($function, $command) {
  $args = func_get_args();
  try {
    $result = call_user_func_array($function, array_slice($args, 2));
    if (is_array($result)) {
      $data = $result;
    }
    elseif (method_exists($result, 'toArray')) {
      $data = $result->getData();
    }
    elseif (method_exists($result, 'getData')) {
      $data = $result->getData();
    }
    else {
      throw new \InvalidArgumentException(t('Result must be an array or object with one of the following methods: toArray(), getData().'));
    }
  }
  catch (\InvalidArgumentException $e) {
    watchdog_exception('solarium', $e);
    $data = array(
      'status' => FALSE,
      'error' => t('Unexpected application error.'),
    );
  }
  catch (\RuntimeException $e) {
    $data = array(
      'status' => FALSE,
      'error' => $e->getMessage(),
    );
  }
  return array(
    '#type' => 'ajax',
    '#commands' => array(
      array(
        'command' => $command,
        'data' => $data,
      ),
    ),
  );
}

/**
 * Returns data about the Solr server.
 *
 * @param Client $client
 *   The Solarium Client object atached to a Solr server.
 *
 * @return array
 *   An associative array of data returned by the Solr server containing:
 *   - status: Always returns OK.
 *   - options: An associative array of server configuration options containing:
 *     - host: Hostname of the Solr server / load balancer.
 *     - key: Unique key of the server configuration.
 *     - path: Path to the Solr application.
 *     - port: Port that the Solr application is listening on.
 *     - scheme: Scheme of the Solr application, e.g. "http", "https"
 *     - timeout: Number of seconds before the connection times out.
 */
function solarium_server_data(Client $client) {
  return array(
    'status' => 'OK',
    'options' => $client->getEndpoint()->getOptions(),
  );
}

/**
 * Pings the Solr server to check it's availability.
 *
 * @param Client $client
 *   The Solarium Client object atached to a Solr server.
 *
 * @return Solarium\QueryType\Ping\Result
 *
 * @throws \RuntimeException
 */
function solarium_server_ping_data(Client $client) {
  $ping = $client->createPing();
  return $client->ping($ping);
}

/**
 * Returns data from the Luke request handler.
 *
 * @param Client $client
 *   The Solarium Client object atached to a Solr server.
 *
 * @return Solarium\QueryType\Luke\Result
 *
 * @throws \RuntimeException
 *
 * @see http://wiki.apache.org/solr/LukeRequestHandler
 */
function solarium_server_luke_data(Client $client) {
  $client->registerQueryType(LukeQuery::QUERY_LUKE, 'Solarium\\QueryType\\Luke\\Query');
  $luke = $client->createQuery(LukeQuery::QUERY_LUKE);
  return $client->execute($luke);
}

/**
 * Returns data from the Stats.jsp request handler.
 *
 * @param Client $client
 *   The Solarium Client object atached to a Solr server.
 *
 * @return Solarium\QueryType\StatsJsp\Result
 *
 * @throws \RuntimeException
 */
function solarium_server_statsjsp_data(Client $client) {
  $client->registerQueryType(StatsJspQuery::QUERY_STATSJSP, 'Solarium\\QueryType\\StatsJsp\\Query');
  $stats_jsp = $client->createQuery(StatsJspQuery::QUERY_STATSJSP);
  return $client->execute($stats_jsp);
}

/**
 * Returns data from the admin/system request handler.
 *
 * @param Client $client
 *   The Solarium Client object atached to a Solr server.
 *
 * @return Solarium\QueryType\System\Result
 *
 * @throws \RuntimeException
 */
function solarium_server_system_data(Client $client) {
  $client->registerQueryType(SystemQuery::QUERY_SYSTEM, 'Solarium\\QueryType\\System\\Query');
  $system = $client->createQuery(SystemQuery::QUERY_SYSTEM);
  return $client->execute($system);
}

/**
 * Returns server status information.
 *
 * This function combines selected output from admin/system, admin/luke, and
 * status.jsp request handlers.
 *
 * @param Client $client
 *   The Solarium Client object atached to a Solr server.
 *
 * @return array
 *
 * @throws \RuntimeException
 *
 * @see solarium_server_luke_data()
 * @see solarium_server_statsjsp_data()
 * @see solarium_server_system_data()
 */
function solarium_server_status_data(Client $client) {
  $luke = solarium_server_luke_data($client);
  $stats_jsp = solarium_server_statsjsp_data($client);
  $system = solarium_server_system_data($client);

  return array();
}
