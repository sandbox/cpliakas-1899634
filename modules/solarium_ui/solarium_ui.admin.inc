<?php

/**
 * @file
 * Administrative page callbacks.
 */

use Solarium\Client;

/**
 * The regex pattern for field arguments.
 */
define('SOLARIUM_FIELD_ARGUMENT_PATTERN', '@^{field:([-_a-zA-Z0-9]+)}$@');

/**
 * Page callback; Displays a list of servers.
 */
function solarium_ui_admin_servers_page() {
  $build = array();

  $header = array(
    'label' => t('Name'),
    'url' => t('URL'),
    'operations' => t('Operations'),
  );

  $rows = array();
  foreach (solarium_get_servers() as $name => $server) {
    $base_path = 'admin/config/search/solarium/servers/' . $name;

    $row = array(
      'label' => check_plain($server->options['label']),
      'url' => check_plain($server->options['url']),
    );

    $operations = array();
    $operations[] = array(
      'title' => t('Edit'),
      'href' => $base_path . '/edit',
    );
    $operations[] = array(
      'title' => t('Status'),
      'href' => $base_path . '/status',
    );
    $operations[] = array(
      'title' => t('Export'),
      'href' => $base_path . '/export',
    );

    $row['operations'] = array(
      'data' => array(
        '#theme' => 'links__node_operations',
        '#links' => $operations,
        '#attributes' => array('class' => array('links', 'inline')),
      ),
    );

    $rows[] = $row;
  }

  $build['servers'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No Solr servers are defined. Add one by clicking the link above.')
  );

  return $build;
}

/**
 * Form for creating and editing a connection to a Solr server.
 *
 * @param stdClass|NULL $server
 *   The loaded server object, FALSE if we are creating a new server.
 *
 * @see solarium_ui_server_form_validate()
 * @see solarium_ui_server_form_submit()
 * @ingroup forms
 */
function solarium_ui_server_form($form, &$form_state, $server = NULL) {
  $form['#is_new'] = empty($server);

  if ($form['#is_new']) {
    ctools_include('export');
    $server = ctools_export_crud_new('solarium_server');
    $server->options = array(
      'label' => '',
      'name' => '',
      'url' => '',
    );
  }

  $form['#solarium_server'] = $server;

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $server->options['label'],
    '#description' => t('A label that describes this server, e.g. "Local", "Production", etc.'),
    '#required' => TRUE,
    '#maxlength' => 255,
    '#size' => 30,
  );

  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => $server->options['name'],
    '#maxlength' => 32,
    '#machine_name' => array(
      'exists' => 'solarium_server_load',
      'source' => array('label'),
    ),
    '#disabled' => !$form['#is_new'],
    '#description' => t('The machine readable name of the Solr server configuration. This value can only contain letters, numbers, and underscores.'),
  );

  $form['url'] = array(
    '#title' => t('Solr server URL'),
    '#type' => 'textfield',
    '#default_value' => $server->options['url'],
    '#description' => t('The URL of the Solr server, e.g. "http://example.com:8983/solr", "http://example.com:8080/solr/core-name".'),
    '#required' => TRUE,
    '#maxlength' => 255,
  );

  $form['actions'] = array(
    '#type' => 'actions'
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/config/search/solarium/servers'),
  );

  $form['#validate'] = array('solarium_ui_server_form_validate');
  $form['#submit'] = array('solarium_ui_server_form_submit');

  return $form;
}

/**
 * Form validation handler for solarium_ui_server_form().
 *
 * @see solarium_ui_server_form_submit()
 */
function solarium_ui_server_form_validate($form, &$form_state) {
  if (!valid_url($form_state['values']['url'], TRUE)) {
    form_set_error('url', t('URL not valid.'));
  }
}

/**
 * Form submission handler for solarium_ui_server_form().
 *
 * @see solarium_ui_server_form_validate()
 */
function solarium_ui_server_form_submit($form, &$form_state) {
  $object = &$form['#solarium_server'];

  form_state_values_clean($form_state);
  $object->name = $form_state['values']['name'];
  $object->options = array_merge($object->options, $form_state['values']);

  try {
    if (!ctools_export_crud_save('solarium_server', $object)) {
      throw new Exception(t('Error saving Solr server connection.'));
    }

    $message = $form['#is_new'] ? t('Solr server connection saved.') : t('Solr server connection updated.');
    drupal_set_message($message);

    $form_state['redirect'] = 'admin/config/search/solarium/servers';
  }
  catch (Exception $e) {
    form_set_error(NULL, $e->getMessage());
    watchdog_exception('solarium_ui', $e);
  }
}

/**
 * Display the server status.
 *
 * @param stdClass $server
 *   The loaded server object, FALSE if we are creating a new server.
 *
 * @ingroup forms
 */
function solarium_ui_server_status_form($form, &$form_state, Client $client) {
  module_load_include('inc', 'solarium', 'solarium.pages');

  return $form;
}

/**
 * Page callback; Displays a list of search pages.
 */
function solarium_ui_admin_search_pages_page() {
  $build = array();

  // Gets servers, abort if no servers are defined.
  $servers = solarium_get_servers();
  if (!$servers) {
    return array(
      '#markup' => t('No Solr servers are defined. At least one is required in order to create a search page.')
    );
  }

  $header = array(
    'label' => t('Name'),
    'url' => t('URL'),
    'server' => t('Server'),
    'operations' => t('Operations'),
  );

  $rows = array();
  foreach (solarium_get_search_pages() as $name => $search_page) {
    $base_path = 'admin/config/search/solarium/search-pages/' . $name;
    $server = $servers[$search_page->options['server']];
    $menu = $search_page->options['menu'];

    $row = array(
      'label' => check_plain($search_page->options['label']),
      'url' => l(str_replace(array('%menu_tail', '%'), '*', $menu['item']), $menu['path']),
      'server' => check_plain($server->options['label']),
    );

    $operations = array();
    $operations[] = array(
      'title' => t('Edit'),
      'href' => $base_path . '/edit',
    );
    $operations[] = array(
      'title' => t('Export'),
      'href' => $base_path . '/export',
    );

    $row['operations'] = array(
      'data' => array(
        '#theme' => 'links__node_operations',
        '#links' => $operations,
        '#attributes' => array('class' => array('links', 'inline')),
      ),
    );

    $rows[] = $row;
  }

  $build['servers'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No search pages are defined. Add one by clicking the link above.'),
  );

  return $build;
}

/**
 * Form for creating and editing a search page.
 *
 * @param stdClass|NULL $search_page
 *   The loaded search page object, FALSE if we are creating a new search page.
 *
 * @see solarium_ui_search_page_form_validate()
 * @see solarium_ui_search_page_form_submit()
 * @ingroup forms
 */
function solarium_ui_search_page_form($form, &$form_state, $search_page = NULL) {
  $form['#is_new'] = empty($search_page);

  if ($form['#is_new']) {
    ctools_include('export');
    $search_page = ctools_export_crud_new('solarium_search_page');
    $search_page->options = array(
      'label' => '',
      'name' => '',
      'title' => '',
      'server' => '',
      'url' => '',
      'menu' => array(),
    );
  }

  $form['#solarium_search_page'] = $search_page;

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $search_page->options['label'],
    '#description' => t('A label used on administrative pages that describes this search page, e.g. "Site search", "Blog search", etc.'),
    '#required' => TRUE,
    '#maxlength' => 255,
    '#size' => 30,
  );

  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => $search_page->options['name'],
    '#maxlength' => 32,
    '#machine_name' => array(
      'exists' => 'solarium_search_page_load',
      'source' => array('label'),
    ),
    '#disabled' => !$form['#is_new'],
    '#description' => t('The machine readable name of the search page configuration. This value can only contain letters, numbers, and underscores.'),
  );

  $form['server'] = array(
    '#title' => t('Solr server'),
    '#type' => 'select',
    '#options' => solarium_ui_get_server_options(),
    '#default_value' => $search_page->options['server'],
    '#description' => t('The Solr server that will be queried for searches on this page.'),
    '#required' => TRUE,
  );

  $form['title'] = array(
    '#title' => t('Search page title'),
    '#type' => 'textfield',
    '#default_value' => $search_page->options['title'],
    '#description' => t('The title of the search page.'),
    '#required' => TRUE,
  );

  $form['url'] = array(
    '#title' => t('Search page URL'),
    '#type' => 'textfield',
    '#default_value' => $search_page->options['url'],
    '#description' => t('The relative URL of the search page. Use <code>{query}</code> as a replacement pattern for where the search keys should be passed via the URL, e.g. search/{query} or search?query={query}.'),
    '#required' => TRUE,
  );

  $form['menu'] = array(
    '#type' => 'value',
    '#value' => $search_page->options['menu'],
  );

  $form['actions'] = array(
    '#type' => 'actions',
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/config/search/solarium/search-pages'),
  );

  $form['#validate'] = array('solarium_ui_search_page_form_validate');
  $form['#submit'] = array('solarium_ui_search_page_form_submit');

  return $form;
}

/**
 * Form validation handler for solarium_ui_search_page_form().
 *
 * @see solarium_ui_search_page_form_submit()
 */
function solarium_ui_search_page_form_validate($form, &$form_state) {
  $url_error = $keywords_in_path = $keywords_in_query = FALSE;
  $field_arguments = array();

  $url = solarium_ui_parse_url($form_state['values']['url']);
  $url_valid = solarium_ui_validate_url($url, $url_error);

  if ($url_valid) {

    // Total number of menu parts.
    $num_parts = count($url['path']) - 1;

    // Builds the path parts.
    $path_parts = array();
    foreach ($url['path'] as $index => $path_part) {
      if (preg_match(SOLARIUM_FIELD_ARGUMENT_PATTERN, $path_part, $match)) {
        $path_parts[] = '%';
        $field_arguments[$match[1]] = array(
          'type' => 'path',
          'index' => $index,
          'field' => $match[1],
        );
      }
      elseif ('{query}' !== $path_part) {
        $path_parts[] = $path_part;
      }
      elseif ($keywords_in_path) {
        $url_error = t('The keyword location can only be specified once.');
        break;
      }
      elseif ($index != $num_parts) {
        $url_error = t('The keyword location must be the last item in the path.');
        break;
      }
      else {
        $path_parts[] = '%menu_tail';
        $keywords_in_path = TRUE;
      }
    }

    // Find the keywords in the query string.
    $keywords_variable = FALSE;
    foreach ($url['query'] as $variable => $value) {
      if (preg_match(SOLARIUM_FIELD_ARGUMENT_PATTERN, $value, $match)) {
        $field_arguments[$match[1]] = array(
          'type' => 'query',
          'variable' => $variable,
          'field' => $match[1],
        );
      }
      elseif ('{query}' == $value) {
        if (!$keywords_in_path && !$keywords_in_query) {
          $keywords_variable = $variable;
        }
        else {
          $url_error = t('The keyword localtion can only be defined once.');
          break;
        }
      }
      else {
        $url_error = t('Hard coded query string variables are not allowed.');
      }
    }
  }

  if (!$url_error) {

    $menu = array(
      'item' => join('/', $path_parts),
      'field arguments' => $field_arguments,
      'keywords in path' => $keywords_in_path,
    );
    if ($keywords_in_path) {
      $menu['keywords index'] = count($url['path']) - 1;
      $menu['tab root'] = str_replace('/%menu_tail', '/%', $menu['item']);
      $menu['tab parent'] = str_replace('/%menu_tail', '', $menu['item']);
      $menu['path'] = $menu['tab parent'];
    }
    else {
      $menu['keywords variable'] = $variable;
      $menu['path'] = $menu['item'];
    }

    // Trigger a menu rebuild if tha page is new or something changed.
    if ($form['#is_new'] || (isset($form['menu']['#value']['item']) && $menu['item'] !=$form['menu']['#value']['item'])) {
      variable_set('menu_rebuild_needed', TRUE);
    }

    form_set_value($form['menu'], $menu, $form_state);
  }
  else {
    form_set_error('url', $url_error);
  }
}

/**
 * Form submission handler for solarium_ui_search_page_form().
 *
 * @see solarium_ui_search_page_form_validate()
 */
function solarium_ui_search_page_form_submit($form, &$form_state) {
  $object = &$form['#solarium_search_page'];

  form_state_values_clean($form_state);
  $object->name = $form_state['values']['name'];
  $object->options = array_merge($object->options, $form_state['values']);

  try {
    if (!ctools_export_crud_save('solarium_search_page', $object)) {
      throw new Exception(t('Error saving search page connection.'));
    }

    $message = $form['#is_new'] ? t('Search page configuration saved.') : t('Search page configuration updated.');
    drupal_set_message($message);

    $form_state['redirect'] = 'admin/config/search/solarium/search-pages';
  }
  catch (Exception $e) {
    form_set_error(NULL, $e->getMessage());
    watchdog_exception('solarium_ui', $e);
  }
}

/**
 * Page callback;
 */
function solarium_ui_admin_recommendation_blocks_page() {
  $build = array();

  return $build;
}

/**
 * Returns an array of server options.
 *
 * @return array
 */
function solarium_ui_get_server_options() {
  $options = array();
  foreach (solarium_get_servers() as $name => $server) {
    $options[$name] = check_plain($server->options['label']);
  }
  return $options;
}

/**
 * @defgroup solarium_ui_url Solarium URL parsers and validators
 *
 * Functions in this group parse and validate the search page URL passed through
 * the search page form.
 *
 * @{
 */

/**
 * Parses the search page URL passed through the admin form.
 *
 * @param string $url
 *   The raw URL passed through the form.
 *
 * @return array|FALSE
 *   An array of URL parts, FALSE if the URL isn't valid.
 */
function solarium_ui_parse_url($url) {
  $parsed_url = parse_url($url);
  if (!$parsed_url) {
    return FALSE;
  }

  // Parse the path parts.
  if (isset($parsed_url['path'])) {
    // Split path by "/", filter empty parts, re-key so parts are zero-based.
    $parsed_url['path'] = array_values(array_filter(explode('/', $parsed_url['path'])));
  }
  else {
    $parsed_url['path'] = array();
  }

  // Parse the query string into associative array.
  if (isset($parsed_url['query'])) {
    parse_str($parsed_url['query'], $query);
    $parsed_url['query'] = $query;
  }
  else {
    $parsed_url['query'] = array();
  }

  return $parsed_url;
}

/**
 * Validated a parsed URL.
 *
 * @param array $url
 *   The parsed URL usually returned by solarium_ui_parse_url().
 * @param string &$errstr
 *   Populated with the validation error.
 *
 * @return boolean
 *   Whether the URL is valid or not.
 *
 * @see solarium_ui_parse_url()
 */
function solarium_ui_validate_url($url, &$errstr = NULL) {
  if (!$url) {
    $errstr = t('Path not valid.');
  }
  elseif (!empty($url['scheme']) || !empty($url['host'])) {
    $errstr = t('Only relative paths are allowed.');
  }
  elseif (empty($url['path'])) {
    $errstr = t('Path to search page is required, e.g. "search", "search/site".');
  }
  elseif (isset($url['query']['q'])) {
    $errstr = t('The query string variable "@var" cannot be used to pass the keywords.', array('@var' => 'q'));
  }
  elseif (isset($url['query']['page'])) {
    $errstr = t('The query string variable "@var" cannot be used to pass the keywords.', array('@var' => 'page'));
  }
  elseif (isset($url['query']['destination'])) {
    $errstr = t('The query string variable "@var" cannot be used to pass the keywords.', array('@var' => 'destination'));
  }
  elseif (!in_array('{query}', $url['path']) && !in_array('{query}', $url['query'])) {
    $errstr = t('The keyword location in the URL must be specified by using the <code>{query}</code> token.');
  }
  else {
    $errstr = '';
  }
  return empty($errstr);
}

/**
 * @} End of "defgroup solarium_ui_url".
 */
