<?php

/**
 * @file
 */

/**
 * Returns predefined connections.
 *
 * @return array
 *   An array keyed by the name of the setting to an array containing:
 *   - label: The label presented to the user.
 *   - url: The predefined URL.
 */
function hook_solarium_ui_predefined_servers() {
  $servers = array();

  $servers['localhost_jetty'] = array(
    'label' => 'Localhost Jetty',
    'url' => 'http://localhost:8983/solr',
  );

  $servers['localhost_tomcat'] = array(
    'label' => 'Localhost Tomcat',
    'url' => 'http://localhost:8080/solr',
  );

  return $servers;
}