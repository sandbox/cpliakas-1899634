<?php

/**
 * @file
 * Page callbacks for the Solarium Search Pages module.
 */

/**
 * Page callback; Search page callback for Solarium.
 *
 * @param string $page_name
 *   The machine name of the search page confguration.
 * @param string $keywords
 *   The search keywords passed through the path, if any.
 */
function solarium_search_page_view($page_name, $keywords = '') {
  $build = array();
  try {

    // Loads the search page configuration.
    $page = solarium_search_page_load($page_name);
    if (!$page) {
      $message = t('Search page "@page" not valid.', array('@page' => $page_name));
      throw new \InvalidArgumentException($message);
    }

    // Extract the keywords from the query string if applicable.
    $menu = $page->options['menu'];
    if (!$menu['keywords in path']) {
      $variable = $menu['keywords variable'];
      $keywords = (isset($_GET[$variable])) ? (string) $_GET[$variable] : '';
    }

    $resultset = solarium_search_page_execute($page, $keywords);
    pager_default_initialize($resultset->getNumFound(), 10);

    $build['search_form'] = drupal_get_form('solarium_search_page_form', $page, $keywords);

    $build['search_results'] = array(
      '#theme' => 'search_results',
      '#results' => solarium_build_results($resultset),
      '#module' => 'solarium_search_page',
    );

  }
  catch (\InvalidArgumentException $e) {
    watchdog('solarium_search_page', $e);
    drupal_not_found();
  }
  catch (\RuntimeException $e) {
    watchdog('solarium_search_page', $e);
    $is_admin = user_access('administer solr servers');
    $message = $is_admin ? check_plain($e->getMessage()) : t('Search is temporarily unavailable. If the problem persists, please contact the site administrator.');
    drupal_set_message($message, 'error');
  }

  return $build;
}

/**
 * Search page form for Solarium searches.
 *
 * @param stdClass $page
 *   The search page configuration.
 * @param string $keywords
 *   The keywords passed by the user.
 *
 * @ingroup forms
 *
 * @see search_form()
 */
function solarium_search_page_form($form, &$form_state, $page, $keywords = '') {

  // Load the core Search CSS file, use the core search module's classes.
  drupal_add_css(drupal_get_path('module', 'search') . '/search.css');
  $form['#id'] = 'search-form';
  $form['#attributes']['class'][] = 'search-form';

  $form['#solarium_search_page'] = $page;

  $form['basic'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('container-inline')),
  );

  $prompt = t('Enter terms');
  $form['basic']['keys'] = array(
    '#type' => 'textfield',
    '#title' => check_plain($prompt),
    '#default_value' => $keywords,
    '#size' => $prompt ? 20 : 40,
    '#maxlength' => 255,
  );

  $button_text = t('Search');
  $form['basic']['submit'] = array(
    '#type' => 'submit',
    '#value' => check_plain($button_text),
  );

  $form['#submit'] = array('solarium_search_page_form_submit');

  return $form;
}

/**
 * Form submission handler for solarium_search_page_form().
 */
function solarium_search_page_form_submit(&$form, &$form_state) {
  $page = $form['#solarium_search_page'];
  $menu = $page->options['menu'];

  $path_parts = explode('/', $menu['path']);
  $query = array();

  // Replace the field arguments with the passed values.
  foreach ($menu['field arguments'] as $field_name => $argument_info) {
    if ('path' == $argument_info['type']) {
      $index = $argument_info['index'];
      $path_parts[$index] = arg($index);
    }
    elseif ('query' == $argument_info['type']) {
      $variable = $argument_info['variable'];
      if (isset($_GET[$variable])) {
        $query[$variable] = (string) $_GET[$variable];
      }
    }
    else {
      watchdog('solarium_search_page', 'Invalid field argument type: @type', array('@type' => $type), WATCHDOG_ERROR);
    }
  }

  // Appends the keywords to the path or sets it as a query string variable.
  if (strlen($form_state['values']['keys']) > 0) {
    if ($menu['keywords in path']) {
      $path_parts[] = $form_state['values']['keys'];
    }
    else {
      $variable = $menu['keywords variable'];
      $query[$variable] = $form_state['values']['keys'];
    }
  }

  $form_state['redirect'] = array(join('/', $path_parts), array('query' => $query));
}
